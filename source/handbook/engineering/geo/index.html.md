---
layout: markdown_page
title: "Geo and DR"
---

## Common Links

- Geo [documentation](https://docs.gitlab.com/ee/gitlab-geo/)
- Issues relating to Geo are mostly to be found on the
[gitlab-ee issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo)
- [Chat channel](https://gitlab.slack.com/archives/geo); please use the `#geo`
chat channel for questions that don't seem appropriate to use the issue tracker
for.


## Planning and Demos

### Planning

Until we can make use of epics and roadmaps within GitLab itself, we are using a 
google sheet to plan the development of Geo and its related feature set around
disaster recovery. Find the sheet in Google Drive by searching for
"GitLab Geo - Project Planning".

### Demos

The Geo team does [demos](/handbook/engineering/#demos) every week at least,
and sometimes more often, to see how well the feature set does against the
Acceptance Criteria that are listed on the "Requirements and Demo Scoring" tab
of the project planning sheet.

The demos are recorded and should be stored in Google Drive under "GitLab Videos
--> [Geo Demos](https://drive.google.com/drive/u/0/folders/1Ot2ElWwEh9vdPx1K8VO5ZMBkxlmRAXm4)".
If you recorded the demo, please make sure the recording ends up in that folder.

---
layout: markdown_page
title: "Legal"
---

## On this page
{:.no_toc}

- TOC
{:toc}
 
## Communication
 
### Issue Trackers
If you need to request legal resources, including contract review, third party license review, legal advice or guidance, please submit all requests through the Legal private issue tracker, by submitting an [email](mailto:incoming+gitlab-legal/legal-issue-tracker@gitlab.com).
 
Please be sure to include sufficient detail regarding your request, including time-sensitive deadlines, relevant documents, and background information necessary to respond.  Through the Legal private issue tracker, you will be updated regarding the status of your request through the Service Desk feature.  The Executive Team will have full access to the [Legal issue tracker](https://gitlab.com/gitlab-legal/legal-issue-tracker).
 
### Chat Channel
Feel free to use the `#legal` chat channel in Slack for general legal questions that don't seem appropriate for the issue tracker or internal email correspondence.  Slack is not to be used for anything that is considered confidential or seeking legal advice.
 
## Contract Templates
 
* [Contracts](https://about.gitlab.com/handbook/contracts/)
* Non-Disclosure Agreement (NDA) (WIP)

### Master Vendor Agreement
All vendors and suppliers doing business with GitLab will require a contract. If the vendor/supplier does not provide an agreement, then GitLab's Master Vendor Agreement can be used. See Vendor Agreement. If using this Master Vendor Agreement, please provide any changes to the template to Legal for revieww. All agreements must be reviewed and approved by Legal before signing.
 
 
## Legal Team Processes
 
* [Signing Legal Documents](https://about.gitlab.com/handbook/signing-legal-documents/)
* [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/)
 
## Company Policies
 
[Anti-harassment](https://about.gitlab.com/handbook/anti-harassment/)
 
 
## General Topics

Frequently Asked Questions (WIP)
 
 
## Other Pages Related to Legal
 
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)
* [General Guidelines](https://about.gitlab.com/handbook/general-guidelines/)
* [Terms](https://about.gitlab.com/terms/)
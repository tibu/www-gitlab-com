---
layout: job_page
title: "Engineering Manager - Production"
---

The Engineering Manager, Production directly manages the developers that run GitLab.com. They see the team as their product. They work on small features or bugs to keep their technical skills sharp and stay familiar with the code. But they emphasize hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and they are always looking to improve the productivity of their team. They must also coordinate across departments to accomplish collaborative goals.

## Responsibilities

- Hire an incredible team that lives our [values](https://about.gitlab.com/handbook/values/)
- Improve the happiness and productivity of the team
- Work on small features and bugs (nothing critical path)
- Manage the agile development and continuous delivery process
- Hold regular 1:1's with team members
- Manage agile projects
- Work across sub-departments within engineering
- Improve the quality, security and performance of the product

## Requirements

- 2-5 years managing software engineering teams
- Demonstrated teamwork in a peak performance organization
- Kubernetes, Docker, Go, or Linux administration
- Experience running a consumer scale platform
- Product company experience
- Enterprise software company experience
- Computer science education or equivalent experience
- Passionate about open source and developer tools
- Exquisite communication skills

## Nice-to-have's

- Online community participation
- Remote work experience
- Startup experience
- Significant open source contributions

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 45 minute first interview with our VP of Engineering
* Next, candidates will be invited to schedule a 45 minute second peer interview with either our Director of Security or Director of Quality
* Next, candidates will be invited to schedule a 45 minute third interview with an Engineering team member
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

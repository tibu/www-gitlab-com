---
layout: job_page
title: "Director of Engineering - Backend"
---

The Director of Engineering, Backend at GitLab manages multiple backend teams in the product development organization. They see their teams as their product. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both developers and managers alike. But they can also grow the existing talent on their teams. Directors are leaders that model the behaviors we want to see in our teams and hold others accountable when necessary. And they create the collaborative and productive environment in which developers and engineering managers do their work.

## Responsibilities

- Hire and manage multiple backend teams that lives our [values](https://about.gitlab.com/handbook/values/)
- Measure and improve the happiness and productivity of the team
- Manage the agile development process
- Drive quarterly OKRs
- Work across sub-departments within engineering
- Own the quality, security and performance of the product
- Write public blog posts and speak at conferences

## Requirements

- 10 years managing multiple software engineering teams
- Experience in a peak performance organization
- Deep Ruby on Rails experience
- Product company experience
- Startup experience
- Enterprise software company experience
- Computer science education or equivalent experience
- Passionate about open source and developer tools
- Exquisite communication skills

## Nice-to-have's

- Kubernetes, Docker, Go, Helm, or Linux administration
- Online community participation
- Remote work experience
- Significant open source contributions

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 45 minute first interview with our VP of Engineering
* Next, candidates will be invited to schedule a 45 minute second peer interview with our Director of Backend
* Next, candidates will be invited to schedule a 45 minute third interview with an Engineering team member
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
